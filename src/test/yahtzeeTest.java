package test;

import static org.junit.Assert.*;
import main.Yahtzee;

import org.junit.Test;

public class yahtzeeTest {
	
	
	
	@Test
	
	public void ones()
	{
		int prova[]={1,1,2,4,4};
		assertEquals(2,Yahtzee.ControlloSomma(prova));
		
	}
	@Test
	public void Twos()
	{
		int prova[]={2,1,2,4,2};
		assertEquals(6,Yahtzee.ControlloSomma1(prova));
		
	}
	
	@Test 
	public void Three()
	{
		int prova[]={3,3,2,4,2};
		assertEquals(6,Yahtzee.ControlloSomma3(prova));
		
	}
	
	@Test
	public void Four()
	{
		int prova[]={1,4,4,2,1};
		assertEquals(8,Yahtzee.ControlloSomma4(prova));
	
	}
	
	@Test
	public void Five()
	{
		int prova[]={1,4,4,2,1};
		assertEquals(0,Yahtzee.ControlloSomma5(prova));
	
	}
	
	@Test
	public void Six()
	{
		int prova[]={6,1,1,2,3};
		assertEquals(6,Yahtzee.ControlloSomma6(prova));
	
	}
	
	@Test
	public void Pair()
	{
		int prova[]={3,3,3,4,4};
		assertEquals(8,Yahtzee.Coppie(prova));
	}
	
	@Test
	public void TwoPair()
	{
		
		int prova[]={1,2,1,4,4};
		assertEquals(10,Yahtzee.Coppie2(prova));
	}
	
	@Test
	public void ThreeofKind()
	{
		
		int prova[]={4,3,5,3,3};
		assertEquals(9,Yahtzee.Three(prova));
	}
	
	@Test
	public void SmallStraight()
	{
		int prova[]={1,3,2,4,1};
		assertEquals(30,Yahtzee.SStraight(prova));
		
	}
	
	@Test
	public void BigStraight()
	{
		int prova[]={5,3,2,4,1};
		assertEquals(40,Yahtzee.BStraight(prova));
		
	}

}
